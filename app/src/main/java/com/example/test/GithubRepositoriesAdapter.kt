package com.example.test

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class GithubRepositoriesAdapter : RecyclerView.Adapter<RepositoryViewHolder>() {

    private val repositories = mutableListOf<GithubRepoModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_repository, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    override fun onBindViewHolder(viewHolder: RepositoryViewHolder, position: Int) {
        viewHolder.bind(repositories[position])
    }

    fun updateData(newData: List<GithubRepoModel>) {
        repositories.clear()
        repositories.addAll(newData)
        notifyDataSetChanged()
    }
}

class RepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val wikiColor = ContextCompat.getColor(itemView.context, R.color.background_wiki)

    private val name: TextView = view.findViewById(R.id.name)
    private val ownerLogin: TextView = view.findViewById(R.id.owner_login)
    private val size: TextView = view.findViewById(R.id.size)

    fun bind(githubRepoModel: GithubRepoModel) {
        name.text = githubRepoModel.name
        ownerLogin.text = githubRepoModel.owner.login
        size.text = githubRepoModel.size.toString()

        val backgroundColor = if (githubRepoModel.has_wiki) wikiColor else Color.TRANSPARENT
        itemView.setBackgroundColor(backgroundColor)
    }
}