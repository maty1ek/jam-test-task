package com.example.test

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {

    @GET("/search/repositories")
    fun searchRepositories(@Query("q") query: String): Single<SearchRepoResponse>
}

data class SearchRepoResponse(val items: List<GithubRepoModel>)