package com.example.test

import io.reactivex.Single
import retrofit2.Retrofit

class GithubRepository(retrofit: Retrofit) {

    private val api = retrofit.create(GithubApi::class.java)

    fun searchRepositories(searchWord: String): Single<SearchRepoResponse> =
        api.searchRepositories(searchWord)
}

