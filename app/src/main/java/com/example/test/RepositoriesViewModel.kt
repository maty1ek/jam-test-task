package com.example.test

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import timber.log.Timber

private const val DEFAULT_SEARCH_QUERY = "tetris"

class RepositoriesViewModel : ViewModel() {

    private val disposable = CompositeDisposable()

    private val repository: GithubRepository =
        GithubRepository(
            NetworkModule().retrofit(OkHttpClient())
        )

    private val internalSearchQuery: MutableLiveData<String> = MutableLiveData(DEFAULT_SEARCH_QUERY)
    val searchQuery: LiveData<String> = internalSearchQuery

    private val internalState: MutableLiveData<State> = MutableLiveData(State.Loading)
    val state: LiveData<State> = internalState

    init {
        searchRepositories(DEFAULT_SEARCH_QUERY)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun searchRepositories(query: String) {
        internalState.value = State.Loading

        internalSearchQuery.value = query

        if (disposable.size() > 0) disposable.clear()

        disposable.add(
            repository.searchRepositories(query)
                .map {
                    it.items
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    internalState.value = State.Success(result)
                    Timber.d("list of repositories :  $result")
                }, {
                    Timber.w("error - $it")
                    internalState.value = State.Error("Oops! Something went wrong")
                })
        )
    }
}

sealed class State {
    data class Error(val message: String) : State()
    object Loading : State()
    data class Success(val list: List<GithubRepoModel>) : State()
}
