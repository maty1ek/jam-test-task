package com.example.test

data class GithubRepoModel(
    val name: String,
    val owner: Owner,
    val size: Int,
    val has_wiki: Boolean
)

data class Owner(val login: String)