package com.example.test

import android.app.Application
import timber.log.Timber

class JamApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}