package com.example.test

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    private lateinit var repositoriesViewModel: RepositoriesViewModel

    private lateinit var messageView: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar

    private val viewAdapter = GithubRepositoriesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        messageView = findViewById(R.id.message)

        repositoriesViewModel = ViewModelProviders.of(this)[RepositoriesViewModel::class.java]

        val searchEditText: EditText = findViewById(R.id.search_query)
        repositoriesViewModel.searchQuery.observe(this, Observer {
            searchEditText.setText(it)
        })
        searchEditText.setOnEditorActionListener { _, actionId, event ->
            if (EditorInfo.IME_ACTION_SEARCH == actionId // Soft-keyboard
                || (event.keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_DOWN)
            ) { // 'Enter' key
                search(searchEditText.text.toString())
                true
            } else {
                false
            }
        }

        findViewById<Button>(R.id.search_button).setOnClickListener {
            search(searchEditText.text.toString())
        }

        recyclerView = findViewById<RecyclerView>(R.id.tetris_repositories)
            .apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = viewAdapter
                addItemDecoration(
                    DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
                )
            }

        progressBar = findViewById(R.id.progress)

        repositoriesViewModel.state.observe(this, Observer {
            handleState(it)
        })
    }

    private fun search(query: String) {
        repositoriesViewModel.searchRepositories(query)
        hideKeyboard()
    }

    private fun showMessage(message: String) {
        messageView.visibility = View.VISIBLE
        messageView.text = message
    }

    private fun handleState(state: State) {
        when (state) {
            is State.Loading -> {
                progressBar.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
                messageView.visibility = View.GONE
            }
            is State.Success -> {
                progressBar.visibility = View.GONE
                messageView.visibility = View.GONE
                if (state.list.isNotEmpty()) {
                    recyclerView.visibility = View.VISIBLE
                    viewAdapter.updateData(state.list)
                } else {
                    recyclerView.visibility = View.GONE
                    showMessage(getString(R.string.empty_list))
                }
            }
            is State.Error -> {
                progressBar.visibility = View.GONE
                recyclerView.visibility = View.GONE
                showMessage(state.message)
            }
        }
    }
}
